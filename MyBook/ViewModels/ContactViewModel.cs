﻿using MyBook.Entity.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBook.ViewModels
{
    public class ContactViewModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string PersonalEmail { get; set; }
        public string WorkEmail { get; set; }
        public string Website { get; set; }
        public DateTime BirthDate { get; set; }
        
        public int AddressID { get; set; }
        public AddressType AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
    }
}