﻿using System;
using MyBook.Data;
using MyBook.Entity;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MyBook.Controllers
{
    public class ContactController : Controller
    {
        private MyBookDataContext db = new MyBookDataContext();

        // GET: Contact
        public ActionResult Index()
        {
            var contacts = db.Contact.Where(x => x.User.ID == 1).ToList();

            if (contacts.Count == 0)
                return HttpNotFound();

            return View(contacts);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                contact.User = db.User.First();
                db.Contact.Add(contact);
                db.SaveChanges();

                return Json(Url.Action("Index", "Contact"));
            }

            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var contact = db.Contact.Find(id);

            if (contact == null)
                return HttpNotFound();

            return View(contact);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var contact = db.Contact.Find(id);

            if (contact == null)
                return HttpNotFound();

            return View(contact);
        }

        [HttpPost]
        public ActionResult Edit(Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;

                int[] deletedIds = contact.Addresses.Where(x => x.AddressID < 0).Select(x => Math.Abs(x.AddressID)).ToArray();
                foreach (var item in deletedIds)
                {
                    db.Address.Remove(db.Address.Single(x => x.AddressID == item));
                }

                for (int i = 0; i < contact.Addresses.Count; i++)
                {
                    var address = contact.Addresses.ElementAt(i);
                    if (address.AddressID == 0)
                    {
                        db.Entry(address).State = EntityState.Added;
                    }
                    else
                    {
                        db.Entry(address).State = EntityState.Modified;
                    }
                }

                db.SaveChanges();

                return Json(Url.Action("Index", "Contact"));
            }

            return View(contact);
        }

        public PartialViewResult GetAddressPartial(Address address)
        {
            return PartialView("_AddressPartial", address ?? new Address());
        }
    }
}