﻿using MyBook.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace MyBook.Data
{
    public class MyBookDataContext : DbContext
    {
        public MyBookDataContext()
            : base("MyBookConnectionString")
        {

        }

        public DbSet<User> User { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Address> Address { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ContactConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }

    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.ToTable("User");
            this.HasKey(x => x.ID);
            this.HasMany(x => x.Contacts)
                .WithRequired(x => x.User);

            Property(x => x.ID)
                .IsRequired();

            Property(x => x.Name)
                .IsRequired();

            Property(x => x.Email)
                .IsRequired();

            Property(x => x.Password)
                .IsRequired();

            Property(x => x.CreateDate)
                .IsRequired();
        }
    }

    public class ContactConfiguration : EntityTypeConfiguration<Contact>
    {
        public ContactConfiguration()
        {
            this.ToTable("Contact");
            this.HasKey(x => x.ID);
            this.HasMany(x => x.Addresses)
                .WithRequired(x => x.Contact);

            Property(x => x.ID)
                .IsRequired();

            Property(x => x.FirstName)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            Property(x => x.LastName)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);
        }
    }

    public class AddressConfiguration : EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        {
            this.ToTable("Address");
            this.HasKey(x => x.AddressID);

            Property(x => x.AddressID)
                .IsRequired();

            Property(x => x.AddressID).HasColumnName("ID");

            Property(x => x.AddressType)
                .IsRequired();

            Property(x => x.AddressLine1)
                .IsRequired();
        }
    }
}