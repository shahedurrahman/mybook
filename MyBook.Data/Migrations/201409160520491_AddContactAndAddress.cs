namespace MyBook.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContactAndAddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        LastName = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        MobilePhone = c.String(),
                        HomePhone = c.String(),
                        WorkPhone = c.String(),
                        PersonalEmail = c.String(),
                        WorkEmail = c.String(),
                        Website = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        User_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AddressType = c.Int(nullable: false),
                        AddressLine1 = c.String(nullable: false),
                        AddressLine2 = c.String(),
                        Street = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        ZipCode = c.String(),
                        Contact_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contact", t => t.Contact_ID, cascadeDelete: true)
                .Index(t => t.Contact_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contact", "User_ID", "dbo.User");
            DropForeignKey("dbo.Address", "Contact_ID", "dbo.Contact");
            DropIndex("dbo.Address", new[] { "Contact_ID" });
            DropIndex("dbo.Contact", new[] { "User_ID" });
            DropTable("dbo.Address");
            DropTable("dbo.Contact");
        }
    }
}
