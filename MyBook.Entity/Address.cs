﻿using MyBook.Entity.Enums;

namespace MyBook.Entity
{
    public class Address
    {
        public int AddressID { get; set; }
        public AddressType AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public Contact Contact { get; set; }
    }
}
