﻿
namespace MyBook.Entity.Enums
{
    public enum AddressType
    {
        Present = 1,
        Permanent = 2,
        Work = 3
    }
}
