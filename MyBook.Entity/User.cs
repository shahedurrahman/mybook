﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBook.Entity
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreateDate { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}